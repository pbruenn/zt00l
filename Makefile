prefix ?= /usr
BINARY = zt00l

install: $(BINARY)
install: version := $(shell awk -F'[()]' '{print $$2; exit}' debian/changelog)
install:
	install --mode=755 -D $(BINARY) "$(DESTDIR)$(prefix)/bin/$(notdir $(BINARY))"
	sed -i 's/DEVELOPMENT_VERSION/${version}/g' "$(DESTDIR)$(prefix)/bin/$(notdir $(BINARY))"
